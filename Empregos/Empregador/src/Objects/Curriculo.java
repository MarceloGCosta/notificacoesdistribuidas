package Objects;

import java.io.Serializable;
import java.util.Scanner;

public class Curriculo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nome;
    private String contato;
    private String area;
    private int cargaHoraria;
    private int salario;

    public Curriculo() {
    }

    public Curriculo(String nome, String contato, String area, int cargaHoraria, int salario) {
        this.nome = nome;
        this.contato = contato;
        this.area = area;
        this.cargaHoraria = cargaHoraria;
        this.salario = salario;
    }

    public void preencherCampos() {

        Scanner teclado = new Scanner(System.in);

        System.out.println("Digite o Nome: ");
        this.setNome(teclado.nextLine());

        System.out.println("Digite o contato: ");
        this.setContato(teclado.nextLine());

        System.out.println("Digite a area de interesse: ");
        this.setArea(teclado.nextLine());

        System.out.println("Digite a carga horaria: ");
        this.setCargaHoraria(teclado.nextInt());
        teclado.nextLine();

        System.out.println("Digite o salario: ");
        this.setSalario(teclado.nextInt());
        teclado.nextLine();

        teclado.close();
    }

    public void print() {
        System.out.println("\n\n\n");
        System.out.println("Nome: " + this.getNome());
        System.out.println("Contato: " + this.getContato());
        System.out.println("area: " + this.getArea());
        System.out.println("Carga Horaria: " + this.getCargaHoraria());
        System.out.println("Salario: " + this.getSalario());
        System.out.println("\n\n\n");
    }

    // #region Getters and Setters
    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}