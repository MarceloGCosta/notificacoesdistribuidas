package Hirer;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Scanner;

import Objects.Curriculo;
import Objects.Vaga;
import java.rmi.RemoteException;
import Interfaces.IHirer2Server;

public class Hirer {
    public static void main(String[] args) throws Exception {

        int dados;

        Registry registroServer = LocateRegistry.getRegistry(9090);
        
        Scanner teclado;

        ServerToHirer server_to_hirer = new ServerToHirer();

        IHirer2Server hirer_to_server = (IHirer2Server) registroServer.lookup("hirer");

        do {

            teclado = new Scanner(System.in);
            
            System.out.flush();
            System.out.println("\n\n\n");
            System.out.println("-1) Sair do Sistema");
            System.out.println("1) Cadastrar uma vaga");
            System.out.println("2) Consultar os curriculos");
            System.out.println("3) Atualizacao automatica");

            dados = teclado.nextInt();
            teclado.nextLine();

            switch (dados) {
                //1) Cadastrar uma vaga
            case 1:
                cadastrarVaga(server_to_hirer,hirer_to_server);
                break;
                //2) Consultar curriculos
            case 2:
                consultarCurriculos(server_to_hirer,hirer_to_server);
                break;
                //3) Atualizacao automatica
            case 3:
                System.out.println("Qual a area de interesse?");
                hirer_to_server.registrarInteresseProfissionais(server_to_hirer, teclado.nextLine());
                break;
            case -1:
                System.out.println("Finalizando Sistema");
                break;
            default:
                System.out.println("Valor invalido");
            }
        } while (dados != -1);
        teclado.close();
    }
    
    static void cadastrarVaga(ServerToHirer server_to_hirer, IHirer2Server hirer_to_server) throws RemoteException{
        Vaga vaga;    
        vaga = new Vaga();
        vaga.preencherCampos();
        hirer_to_server.cadastroVagas(server_to_hirer, vaga);
    }
    
    static void consultarCurriculos(ServerToHirer server_to_hirer,IHirer2Server hirer_to_server) throws RemoteException{
        Curriculo curriculo;
        Scanner teclado;
        
        teclado = new Scanner(System.in);
        
        curriculo = new Curriculo();
        System.out.println("\nDigite a AREA de interesse: ");
        curriculo.setArea(teclado.nextLine());

        System.out.println("\nDigite o SALARIO ofertado: ");
        curriculo.setSalario(teclado.nextInt());
        teclado.nextLine();

        ArrayList<Curriculo> resp = hirer_to_server.consultaCurriculos(server_to_hirer, curriculo);
        if (!resp.isEmpty()) {
            resp.forEach((x) -> {
                x.print();
            });
        } else {
            //Colocar o número de curriculos disponíveis... 
            System.out.println("Sem curriculos na base");
        }    
    }
}