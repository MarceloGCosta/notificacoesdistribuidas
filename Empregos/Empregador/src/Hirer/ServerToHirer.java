package Hirer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import Interfaces.IServer2Client;

//Class used to be the bridge between the server -> client
public class ServerToHirer extends UnicastRemoteObject implements IServer2Client {

    //Constructor
    protected ServerToHirer() throws RemoteException {
        super();
    }

    private static final long serialVersionUID = -2564510475347059842L;

    //prints the notification at the screen
    @Override
    public void echo(String msg) throws RemoteException {
        System.out.println("Notificação:" + msg);
    }
}