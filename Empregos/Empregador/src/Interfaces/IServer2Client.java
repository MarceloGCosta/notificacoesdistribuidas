package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServer2Client extends Remote {
    public void echo(String msg) throws RemoteException;
}