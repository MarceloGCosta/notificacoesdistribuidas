package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import Objects.Curriculo;
import Objects.Vaga;

public interface IHirer2Server extends Remote {
    public void chamar(String msg, IServer2Client interfaceCli) throws RemoteException;

    public void cadastroVagas(IServer2Client referenciaCli, Vaga vaga) throws RemoteException;

    public ArrayList<Curriculo> consultaCurriculos(IServer2Client referenciaCli, Curriculo curriculo) throws RemoteException;

    public void registrarInteresseProfissionais(IServer2Client referenciaCli, String areaOferta) throws RemoteException;
}