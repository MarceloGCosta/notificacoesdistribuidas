/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import Interfaces.IServer2Client;

/**
 *
 * @author usuario
 */
public class Subscription {
    private SubsType Type;
    private String AreaInteresse;
    private IServer2Client cliRef;

    public SubsType getType() {
        return Type;
    }

    public void setType(SubsType Type) {
        this.Type = Type;
    }

    public Subscription(SubsType Type, String AreaInteresse, IServer2Client cliRef) {
        this.Type = Type;
        this.AreaInteresse = AreaInteresse;
        this.cliRef = cliRef;
    }

    public String getAreaInteresse() {
        return AreaInteresse;
    }

    public void setAreaInteresse(String AreaInteresse) {
        this.AreaInteresse = AreaInteresse;
    }

    public IServer2Client getCliRef() {
        return cliRef;
    }

    public void setCliRef(IServer2Client cliRef) {
        this.cliRef = cliRef;
    }
}
