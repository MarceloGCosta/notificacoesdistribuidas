
package Objects;

public enum SubsType {
    Curriculo('c'),
    Vaga('v');
    
    public char valor;

    SubsType(char type) {
        valor = type;
    }
}