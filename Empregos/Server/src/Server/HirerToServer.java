package Server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import Objects.Curriculo;
import Objects.Vaga;
import Interfaces.IHirer2Server;
import Interfaces.IServer2Client;

public class HirerToServer extends UnicastRemoteObject implements IHirer2Server {

    private MySQL base;

    protected HirerToServer(MySQL base) throws RemoteException {
        super();
        this.base = base;
    }

    private static final long serialVersionUID = 2241569544740392197L;

    @Override
    public void chamar(String msg, IServer2Client interfaceCli) throws RemoteException {
        System.out.println("Recebido: " + msg);
        interfaceCli.echo(msg);
    }

    @Override
    public void cadastroVagas(IServer2Client referenciaCli, Vaga vaga) throws RemoteException {
        this.base.addVaga(vaga);
        this.base.notifyVagas(vaga);
        System.out.println("Vaga cadastrada");
    }

    @Override
    public ArrayList<Curriculo> consultaCurriculos(IServer2Client referenciaCli, Curriculo curriculo)
            throws RemoteException {
        return this.base.consultaCurriculo(curriculo);

    }

    @Override
    public void registrarInteresseProfissionais(IServer2Client referenciaCli, String areaOferta) throws RemoteException {
        this.base.addCandidatosSubscribers(areaOferta, referenciaCli);
    }

}