package Server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
    public static void main(String[] args) throws Exception {       
        
        //registro de nomes
        Registry referenciaServicoNomes = LocateRegistry.createRegistry(9090);
        
        //base de dados
        MySQL base = new MySQL();

        //obj que será o receptor de comandos dos candidatos
        WorkerToServer worker = new WorkerToServer(base);
        
        //obj que será o receptor de comandos das agencias
        HirerToServer hirer = new HirerToServer(base);

        //link the "APIs" with the channels
        referenciaServicoNomes.bind("worker", worker);
        referenciaServicoNomes.bind("hirer", hirer);

        System.out.println("SERVER ON");
    }
}