package Server;

import java.rmi.RemoteException;
import java.util.ArrayList;

import Objects.*;
import Interfaces.IServer2Client;

//Nosso SGBD capenga
public class MySQL {
    //tabela de curriculos
    private ArrayList<Curriculo> curriculos;
    
    //tabela de vagas
    private ArrayList<Vaga> vagas;
    
    //tabela de subscribers
    private ArrayList<Subscription> subscribers;

    //constructor
    public MySQL() {
        this.curriculos = new ArrayList<>();
        this.vagas = new ArrayList<>();
        this.subscribers = new ArrayList<>();
    }

    //getters & setters curriculo
    public void addCuriculo(Curriculo curriculo) {
        this.curriculos.add(curriculo);
    }
    public void removeCuriculo(Curriculo curriculo) {
        this.curriculos.remove(curriculo);
    }
    public String listCurriculos() {
        return this.curriculos.toString();
    }
    public ArrayList<Curriculo> consultaCurriculo(Curriculo curriculo) {
        ArrayList<Curriculo> temp = new ArrayList<>();

        for (Curriculo candidato : this.curriculos) {
            if (candidato.getArea().equalsIgnoreCase(curriculo.getArea())
                    && curriculo.getSalario() >= candidato.getSalario())
                temp.add(candidato);
        }

        return temp;
    }

    //getters & setters vaga
    public void addVaga(Vaga vaga) {
        this.vagas.add(vaga);
    }
    public void removeVaga(Vaga vaga) {
        this.vagas.remove(vaga);
    }
    public String listVagas() {
        return this.vagas.toString();
    }
    public ArrayList<Vaga> consultaVaga(Vaga vaga) {
        ArrayList<Vaga> temp = new ArrayList<>();

        for (Vaga oferta : this.vagas) {
            if (oferta.getArea().equalsIgnoreCase(vaga.getArea()) && vaga.getSalario() <= oferta.getSalario())
                temp.add(oferta);
        }

        return temp;
    }

    //cria uma subscription para quem se interessa por curriculos
    public void addCandidatosSubscribers(String area, IServer2Client cli) {
        this.subscribers.add(new Subscription(SubsType.Curriculo, area, cli));
    }

    //cria uma subscription para quem se interessa por vagas
    public void addVagasSubscribers(String area, IServer2Client cli) {
        this.subscribers.add(new Subscription(SubsType.Vaga, area, cli));
    }

    //retorna os workers que se interessam por aquela vaga 
    public ArrayList<IServer2Client> getVagasSubscribers(String area) {
        ArrayList<IServer2Client> subList = new ArrayList<>();
        for(Subscription sub : this.subscribers){
            if(sub.getType() == SubsType.Vaga && sub.getAreaInteresse().equals(area)){
                subList.add(sub.getCliRef());
            } 
        }
        return subList;
    }

    //retorna os hirers que se interessam por aquele curriculo
    public ArrayList<IServer2Client> getCandidatosSubscribers(String area) {
        ArrayList<IServer2Client> subList = new ArrayList<>();
        for(Subscription sub : this.subscribers){
            if(sub.getType() == SubsType.Curriculo && sub.getAreaInteresse().equals(area)){
                subList.add(sub.getCliRef());
            } 
        }
        return subList;
    }

    //notifica os workers das vagas
    public void notifyVagas(Vaga vaga) throws RemoteException {
        ArrayList<IServer2Client> list = getVagasSubscribers(vaga.getArea());
        
        if (list != null) {
            for (IServer2Client cli : list) {
                cli.echo(" Nova vaga em " + vaga.getArea());
            }
        }
    }
    
    //notifica os hirers dos curriculos
    public void notifyCandidatos(Curriculo curriculo) throws RemoteException {
        ArrayList<IServer2Client> list = getCandidatosSubscribers(curriculo.getArea());
        
        if (list != null) {
            for (IServer2Client cli : list) {
                cli.echo(" Novo curriculo de " + curriculo.getArea());
            }
        }
    }
}