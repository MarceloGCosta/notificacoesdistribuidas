package Server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import Objects.Curriculo;
import Objects.Vaga;
import Interfaces.IServer2Client;
import Interfaces.IWorker2Server;

public class WorkerToServer extends UnicastRemoteObject implements IWorker2Server {

    private static final long serialVersionUID = 2241569544740392197L;

    private MySQL base;

    protected WorkerToServer(MySQL base) throws RemoteException {
        super();
        this.base = base;
    }

    @Override
    public void chamar(String msg, IServer2Client interfaceCli) throws RemoteException {
        System.out.println("Recebido: " + msg);
        interfaceCli.echo(msg);
    }

    @Override
    public void cadastroCurriculo(IServer2Client referenciaCli, Curriculo curriculo) throws RemoteException {
        this.base.addCuriculo(curriculo);
        this.base.notifyCandidatos(curriculo);
    }

    @Override
    public ArrayList<Vaga> consultaVagas(IServer2Client referenciaCli, Vaga vaga) throws RemoteException {
        return this.base.consultaVaga(vaga);
    }

    @Override
    public void registrarInteresseVagas(IServer2Client referenciaCli, String areaInteresse) throws RemoteException {
        this.base.addVagasSubscribers(areaInteresse, referenciaCli);
    }

}