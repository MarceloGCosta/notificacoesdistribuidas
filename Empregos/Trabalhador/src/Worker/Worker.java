package Worker;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Scanner;

import Objects.Curriculo;
import Objects.Vaga;
import java.rmi.RemoteException;
import Interfaces.IWorker2Server;

public class Worker {
    public static void main(String[] args) throws Exception {
        try{
            //System.setSecurityManager(null);

            Registry registroServer = LocateRegistry.getRegistry(9090);

            Vaga vaga;
            Scanner keyboard = new Scanner(System.in);

            ServerToWorker server_to_worker = new ServerToWorker();
            IWorker2Server worker_to_server = (IWorker2Server) registroServer.lookup("worker");

            int input;

            do {
                System.out.flush();

                System.out.println("Selecione uma opção:");

                System.out.println("1) CADASTRO DE CURRICULO");
                System.out.println("2) CONSULTA DE VAGAS");
                System.out.println("3) REGISTRAR INTERESSE EM VAGAS");

                System.out.println("\n0) SAIR\n\n");
                input = keyboard.nextInt();
                keyboard.nextLine();
    
                switch (input) {
                case 1:
                    cadastroCurriculo(server_to_worker, worker_to_server);
                    break;
                case 2:
                    // consultar vagas
                    vaga = new Vaga();

                    System.out.println("Qual a area de interesse?");
                    vaga.setArea(keyboard.nextLine());

                    System.out.println("Qual o salario minimo?");
                    vaga.setSalario(keyboard.nextInt());
                    keyboard.nextLine();

                    ArrayList<Vaga> resp = worker_to_server.consultaVagas(server_to_worker, vaga);
                    if (!resp.isEmpty()) {
                        resp.forEach((x) -> {
                            x.print();
                        });
                    } else {
                        System.out.println("Nenhuma vaga Encontrada");
                    }

                    break;
                case 3:
                    // receber updates
                    System.out.println("Qual a area de interesse?");
                    worker_to_server.registrarInteresseVagas(server_to_worker, keyboard.nextLine());
                    break;
                case -1:
                    System.out.println("Finalizando Sistema");
                    break;
                default:
                    System.out.println("Apenas valores entre 1 e 3");
                }
            } while (input != -1);

            keyboard.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }   
    // cadastrar curriculo    
    static void cadastroCurriculo(ServerToWorker server_to_worker, IWorker2Server worker_to_server) throws RemoteException{

        Curriculo curriculo = new Curriculo();
        curriculo.preencherCampos();
        worker_to_server.cadastroCurriculo(server_to_worker, curriculo);
    }
    // consultar vagas
    static void consultarVaga(ServerToWorker server_to_worker, IWorker2Server worker_to_server) throws RemoteException{

        Scanner teclado = new Scanner(System.in);                    
    
        Vaga vaga = new Vaga();
        
        System.out.println("Qual a area de interesse?");
        vaga.setArea(teclado.nextLine());

        System.out.println("Qual o salario minimo?");
        vaga.setSalario(teclado.nextInt());
        teclado.nextLine();

        ArrayList<Vaga> resp = worker_to_server.consultaVagas(server_to_worker, vaga);
        if (!resp.isEmpty()) {
            resp.forEach((x) -> {
                x.print();
            });
        } else {
            System.out.println("Nenhuma vaga Encontrada");
        }
    }
}
