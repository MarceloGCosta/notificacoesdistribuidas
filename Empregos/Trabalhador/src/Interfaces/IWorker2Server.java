package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import Objects.Curriculo;
import Objects.Vaga;

public interface IWorker2Server extends Remote {
    public void chamar(String msg, IServer2Client interfaceCli) throws RemoteException;

    public void cadastroCurriculo(IServer2Client referenciaCli, Curriculo curriculo) throws RemoteException;

    public ArrayList<Vaga> consultaVagas(IServer2Client referenciaCli, Vaga vaga) throws RemoteException;

    public void registrarInteresseVagas(IServer2Client referenciaCli, String areaInteresse) throws RemoteException;

}