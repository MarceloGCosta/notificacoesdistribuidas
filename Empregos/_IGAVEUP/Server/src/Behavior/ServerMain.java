package Behavior;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServerMain {
    public static void main(String[] args) throws Exception {
        
        System.setSecurityManager(null);
        
        Registry referenciaServicoNomes = LocateRegistry.createRegistry(9090);
        
        //instancia base de dados
        Base base = new Base();
        
        CandidatoImpl candidato = new CandidatoImpl(base);
        
        referenciaServicoNomes.bind("candidato", candidato);
        
        AgenciaImpl agencia = new AgenciaImpl();
        referenciaServicoNomes.bind("agencia", agencia);
        
        System.out.println("Server Running...");
    }
}